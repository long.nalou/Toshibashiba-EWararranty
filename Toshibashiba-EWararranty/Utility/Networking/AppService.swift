//
//  AppService.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/8/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import Foundation
import Alamofire


struct AppService {
    private init() {}
    static func loginUser(username: String, 
                          password: String, 
                          completion: @escaping (_ isSuccess: Bool,_ error: Error?) -> Void) {
        Alamofire.request(NetworkRouter.loginUser(username: username, password: password)).responseJSON { (response) in
            switch (response.result){
            case let .success(value):
                let dataDict = value as! NSDictionary
                if dataDict["status"] as? String == "success" {
                    ObjectManager.default.token = dataDict["Token"] as? String ?? ""
                    completion(true, nil)
                }else {
                    let userInfo: [AnyHashable : Any] = [
                            NSLocalizedDescriptionKey :  NSLocalizedString("Unauthorized", value: dataDict["message"] as? String ?? "Login Fail", comment: "") ,
                            NSLocalizedFailureReasonErrorKey : NSLocalizedString("Unauthorized", value: dataDict["message"] as? String ?? "Login Fail", comment: "")]
                    let error = NSError(domain: "Error", code: 0, userInfo: userInfo as? [String : Any])
                    completion(false, error)
                }
            case let .failure(error):
                completion(false, error)
            }
        }
    }
    
    
    static func changePassword(username: String, 
                               oldPassword: String, 
                               newPassword: String , 
                               completion: @escaping (_ isSuccess: Bool,_ error: Error?) -> Void) {
        Alamofire.request(NetworkRouter.changePass(username: username, oldPassword: oldPassword, newPassWord: newPassword)).responseJSON { (response) in
            switch (response.result){
            case let .success(value):
                let dataDict = value as! NSDictionary
                if dataDict["status"] as? String == "success" {
                    completion(true, nil)
                }else {
                    let userInfo: [AnyHashable : Any] = [
                        NSLocalizedDescriptionKey :  NSLocalizedString("Unauthorized", value: dataDict["message"] as? String ?? "Change password Fail" , comment: "") ,
                        NSLocalizedFailureReasonErrorKey : NSLocalizedString("Unauthorized", value: dataDict["message"] as? String ?? "Change password Fail", comment: "")]
                    let error = NSError(domain: "Error", code: 0, userInfo: userInfo as? [String : Any])
                    completion(false, error)
                }
            case let .failure(error):
                completion(false, error)
            }
        }
    }
    
    
    static func validTelephoneNumber(telephoneNumber: String,
                                     completion: @escaping (_ isSuccess: Bool,_ error: Error?) -> Void) {
        Alamofire.request(NetworkRouter.validTelephoneNumber(telephoneNumber: telephoneNumber)).responseJSON { (response) in
            switch (response.result){
            case let .success(value):
                let dataDict = value as! NSDictionary
                if dataDict["status"] as? String == "success" {
                    completion(true, nil)
                }else {
                    let userInfo: [AnyHashable : Any] = [
                        NSLocalizedDescriptionKey :  NSLocalizedString("Unauthorized", value: dataDict["message"] as? String ?? "telephone number invalid", comment: "") ,
                        NSLocalizedFailureReasonErrorKey : NSLocalizedString("Unauthorized", value: dataDict["message"] as? String ?? "telephone number invalid", comment: "")]
                    let error = NSError(domain: "Error", code: 0, userInfo: userInfo as? [String : Any])
                    completion(false, error)
                }
            case let .failure(error):
                completion(false, error)
            }
        }
    }
    
    static func customerRegistration (model1: String, 
                                      serial: String, 
                                      model2: String, 
                                      customerFirstName: String, 
                                      customerLastName: String, 
                                      telephone: String, completion: @escaping (_ isSuccess: Bool,_ error: Error?) -> Void) {
        Alamofire.request(NetworkRouter.customerRegistration(model1: model1, serial: serial, model2: model2, customerFirstName: customerFirstName, customerLastName: customerLastName, telephone: telephone)).responseJSON { (response) in
            switch (response.result){
            case let .success(value):
                let dataDict = value as! NSDictionary
                if dataDict["status"] as? String == "success" {
                    completion(true, nil)
                }else {
                    let userInfo: [AnyHashable : Any] = [
                        NSLocalizedDescriptionKey :  NSLocalizedString("Unauthorized", value: dataDict["message"] as? String ?? "Registration fail", comment: "") ,
                        
                        NSLocalizedFailureReasonErrorKey : NSLocalizedString("Unauthorized", value: dataDict["message"] as? String ?? "Registration fail", comment: "")]
                    let error = NSError(domain: "Error", code: 0, userInfo: userInfo as? [String : Any])
                    completion(false, error)
                }
            case let .failure(error):
                completion(false, error)
            }
        }
    }
    
    static func validSerialNumber(serialNumber: String,
                                  completion: @escaping (_ isSuccess: Bool,_ listProduct: [ProductModel],_ error: Error?) -> Void) {
        Alamofire.request(NetworkRouter.validSerialNumber(serialNumber: serialNumber)).responseJSON { (response) in
            var listProduct = [ProductModel]()
            switch (response.result){
            case let .success(value):
                let dataDict = value as! NSDictionary
                if dataDict["status"] as? String == "success", 
                    let modelList = dataDict["modelList"] as? NSArray {
                    for item in modelList {
                        let product = ProductModel(json: item as? NSDictionary)
                        if product.status == "success" {
                            listProduct.append(product)
                        }
                    }
                    completion(true,listProduct, nil)
                }else {
                    let userInfo: [AnyHashable : Any] = [
                        NSLocalizedDescriptionKey :  NSLocalizedString("Unauthorized", value: dataDict["message"] as? String ?? "telephone number invalid", comment: "") ,
                        NSLocalizedFailureReasonErrorKey : NSLocalizedString("Unauthorized", value: dataDict["message"] as? String ?? "telephone number invalid", comment: "")]
                    let error = NSError(domain: "Error", code: 0, userInfo: userInfo as? [String : Any])
                    completion(false,listProduct, error)
                }
            case let .failure(error):
                completion(false, listProduct,error)
            }
        }
    }
    
}


//
//  RootViewController.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/8/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import UIKit

enum RootTab {
    case products
    case register
    case settings
    
    var title: String {
        switch self {
        case .products:
            return "Danh sách kích hoạt"
        case .register:
            return "Kích hoạt bảo hành"
        case .settings:
            return "Cài đặt"
        }
    }
    
    var icon: UIImage? {
        switch self {
        case .products:
            return UIImage(named: "list_inactive")
        case .register:
            return UIImage(named: "dashboard_inactive")
        case .settings:
            return UIImage(named: "settings_inactive")
        }
    }
    
    var selected_icon: UIImage? {
        switch self {
        case .products:
            return UIImage(named: "list_active")
        case .register:
            return UIImage(named: "dashboard_active")
        case .settings:
            return UIImage(named: "settings_active")
        }
    }
    
    var controller: UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        switch self {
        case .products:
            
            let nextVC = storyboard.instantiateViewController(withIdentifier: "ListProductViewController") as! ListProductViewController
            var navigationController = UINavigationController(rootViewController: nextVC)
            
            return navigationController
        case .register:
            var nextVC = storyboard.instantiateViewController(withIdentifier: "RegisterProductViewController") as! RegisterProductViewController
            
            let navigationController = UINavigationController(rootViewController: nextVC)
            
            return navigationController
        
        case .settings:
            let nextVC = storyboard.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
            
            var navigationController = UINavigationController(rootViewController: nextVC)
            
            return navigationController
        }
    }
    
    var index: Int {
        switch self {
        case .products:
            return 1
        case .register:
            return 0
        case .settings:
            return 2
        }
    }
    
    static let tabItems: [RootTab] = [ .register, .products, .settings]
}

class RootViewController: UITabBarController, UITabBarControllerDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        tabBar.isTranslucent = true
        
        setupTabs()
    }
    


    
    func setupTabs() {
        let style = NSMutableParagraphStyle()
        style.minimumLineHeight = 20
        style.alignment = .center
        
        RootTabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: ColorHelper.red, NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedStringKey.paragraphStyle: style], for:.selected)
        
        RootTabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 10), NSAttributedStringKey.paragraphStyle: style], for:.normal)
        
        viewControllers = RootTab.tabItems.map {
            let controller = $0.controller
            controller.tabBarItem = RootTabBarItem(tab: $0)
            return controller
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

class RootTabBarItem: UITabBarItem {
    let tab: RootTab
    init(tab: RootTab) {
        self.tab = tab
        super.init()
        
        title = tab.title
        image = tab.icon
        selectedImage = tab.selected_icon
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setLocalizedText() {
        title = tab.title
    }
    
}

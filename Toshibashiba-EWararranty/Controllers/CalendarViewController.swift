//
//  CalendarViewController.swift
//  Tosiba DS
//
//  Created by Dung Nguyen on 8/19/18.
//  Copyright © 2018 Chris Nguyen. All rights reserved.
//

import UIKit
import GCCalendar
import MaterialComponents

class CalendarViewController: UIViewController {
    
    @IBOutlet weak var calendarView: GCCalendarView!
    
    var _selectedDate: Date?
    
    typealias ReturnSelectedDate = (Date) -> ()
    var selectedDate: ReturnSelectedDate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = MDCPalette.grey.tint50
        calendarView.backgroundColor = MDCPalette.grey.tint50
        calendarView.delegate = self
        calendarView.displayMode = .month
        
        setupNavbar ()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupNavbar () {
        let navBar: UINavigationBar = UINavigationBar(frame: CGRect(x: 0, y: 20, width: view.frame.width, height: 44))
        navBar.barTintColor = MDCPalette.grey.tint50
        self.view.addSubview(navBar)
        let navItem = UINavigationItem(title: Helper.formatDateToString(date: self._selectedDate!, format: "dd/MM/yyyy"))
        let doneItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: nil, action: #selector(self.doneSelectDateFunc))
        navItem.rightBarButtonItem = doneItem
        navItem.rightBarButtonItem?.tintColor = MDCPalette.green.tint500
        
        navBar.setItems([navItem], animated: false)
    }
    
    @objc func doneSelectDateFunc () {
        // Callback from TransactionViewController
        if let selectedDate = selectedDate {
            selectedDate(_selectedDate!)
        }
        
//        self.hero.dismissViewController()
        self.dismiss(animated: true, completion: nil)
    }
}

extension CalendarViewController: GCCalendarViewDelegate {
    func calendarView(_ calendarView: GCCalendarView, didSelectDate date: Date, inCalendar calendar: Calendar) {
        self._selectedDate = date
        setupNavbar()
    }
}

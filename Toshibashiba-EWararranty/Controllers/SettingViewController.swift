//
//  SettingViewController.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/9/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SettingViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "ACCOUNT"
        case 1:
            return "OTHER"
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        
        
        if indexPath.section == 0 {
            cell.tilteLabel.text = "Thay đổi mật khẩu"
            cell.avatarImage.image = UIImage(named: "icon_changePass")
        }else if indexPath.section == 1 {
            cell.tilteLabel.text = "Đăng xuất"
            cell.avatarImage.image = UIImage(named: "icon_logOut")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                self.performSegue(withIdentifier: "ChangePassword", sender: nil)
            }
            
        case 1: 
            if indexPath.row == 0 {
                UserDefaults.standard.removeObject(forKey:"token")
                self.dismiss(animated: true, completion: nil)
            }
        default:
            break
        }
    }
}

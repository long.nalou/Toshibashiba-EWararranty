//
//  ScannerViewController.swift
//  Tosiba DS
//
//  Created by Dung Nguyen on 8/19/18.
//  Copyright © 2018 Chris Nguyen. All rights reserved.
//

import AVFoundation
import UIKit
import MaterialComponents

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var type: String = ""
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let systemSoundId : SystemSoundID = 1016
    
    //captureSession manages capture activity and coordinates between input device and captures outputs
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    //Empty Rectangle with green border to outline detected QR or BarCode
    let codeFrame:UIView = {
        let codeFrame = UIView()
        codeFrame.layer.borderColor = UIColor.red.cgColor
        codeFrame.layer.borderWidth = 1.5
        codeFrame.frame = CGRect.zero
        codeFrame.translatesAutoresizingMaskIntoConstraints = false
        codeFrame.tag = 100
        return codeFrame
    }()
    
    override func viewDidDisappear(_ animated: Bool) {
        captureSession?.stopRunning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        captureSession?.startRunning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(type)
        
        //AVCaptureDevice allows us to reference a physical capture device (video in our case)
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        if let captureDevice = captureDevice {
            
            do {
                
                captureSession = AVCaptureSession()
                
                // CaptureSession needs an input to capture Data from
                let input = try AVCaptureDeviceInput(device: captureDevice)
                captureSession?.addInput(input)
                
                // CaptureSession needs and output to transfer Data to
                let captureMetadataOutput = AVCaptureMetadataOutput()
                captureSession?.addOutput(captureMetadataOutput)
                
                //We tell our Output the expected Meta-data type
                captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                
                captureSession?.startRunning()
                
                //The videoPreviewLayer displays video in conjunction with the captureSession
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                videoPreviewLayer?.videoGravity = .resizeAspectFill
                videoPreviewLayer?.frame = view.layer.bounds
                view.layer.addSublayer(videoPreviewLayer!)
                view.bringSubview(toFront: infoLbl)
                
                backBtn.layer.cornerRadius = 5
                backBtn.layer.borderWidth = 1
                backBtn.layer.borderColor = UIColor.white.cgColor
                view.bringSubview(toFront: backBtn)
            }
                
                
            catch {
                //                delegate.log.error("Error in ScannerViewController")
            }
        }
        
    }
    
    // the metadataOutput function informs our delegate (the ScannerViewController) that the captureOutput emitted a new metaData Object
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            //            delegate.log.warning("No objects returned")
            return
        }
        
        let metaDataObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        guard var StringCodeValue = metaDataObject.stringValue else {
            return
        }
        
        view.addSubview(codeFrame)
        
        //transformedMetaDataObject returns layer coordinates/height/width from visual properties
        guard let metaDataCoordinates = videoPreviewLayer?.transformedMetadataObject(for: metaDataObject) else {
            return
        }
        
        //Those coordinates are assigned to our codeFrame
        codeFrame.frame = metaDataCoordinates.bounds
        AudioServicesPlayAlertSound(systemSoundId)
        
        if StringCodeValue.contains("http://") {
            StringCodeValue = StringCodeValue.replacingOccurrences(of: "http://", with: "https://")
        } else {
            StringCodeValue = "https://google.com/search?q=\(StringCodeValue)"
        }
        
        if let url = URL(string: StringCodeValue) {
            
            //            let detailScannerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailScannerVC") as! DetailScannerViewController
            //            detailScannerVC.scannedCode = url
            //            detailScannerVC.hero.isEnabled = true
            //            detailScannerVC.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
            //
            //            detailScannerVC.returnWithContinueScan = { () in
            //                if let viewWithTag = self.view.viewWithTag(100) {
            //                    viewWithTag.removeFromSuperview()
            //                }
            //                self.captureSession?.startRunning()
            //            }
            //
            //            self.present(detailScannerVC, animated: true, completion: nil)
            
            captureSession?.stopRunning()
        }
    }
    
    @IBAction func backBtnClick(_ sender: UIButton) {
//        self.hero.dismissViewController()
        self.dismiss(animated: true, completion: nil)
    }
    
}

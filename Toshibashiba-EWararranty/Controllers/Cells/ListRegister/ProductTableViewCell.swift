//
//  ProductTableViewCell.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/9/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    
    static let identifier:String = "ProductCell"
    class func registerWith(tableView:UITableView) {
        tableView.register(UINib(nibName: "ProductTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: identifier)
    }
    
    @IBOutlet weak var lbSerialNumber: UILabel!
    @IBOutlet weak var lbModelNumber: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

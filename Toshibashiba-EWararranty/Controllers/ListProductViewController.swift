//
//  ListProductViewController.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/8/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import UIKit



class ListProductViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var listDateCollectionView: UICollectionView!
    
    var listDate: [String] = []
    var listShow: [Bool] = []
    var listShowDay: [Bool] = []
    var now: String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initListNameDate()
        self.initListDay()
        self.initCLV()
        self.initTBV()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var rowCell = 0
        self.now = self.formatDate(date: Date())
        for i in 0 ..< self.listDate.count {
            if self.listDate[i] == now {
                rowCell = i
                break
            }
        }
        self.listShow[rowCell] = true
        self.listDateCollectionView.scrollToItem(at: IndexPath(row: rowCell, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initCLV() {
        DateListCollectionViewCell.regiterWith(collectionView: self.listDateCollectionView)
        self.listDateCollectionView.delegate = self
        self.listDateCollectionView.dataSource = self
    }
    
    func initTBV() {
        ProductTableViewCell.registerWith(tableView: self.tableView)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}

// MARK: init data
extension ListProductViewController {
    
    func initListNameDate() {
        for i in 1 ... 12 {
            var month = ""
            if i > 9 {
                month = "\(i)"
            } else {
                month = "0" + "\(i)"
            }
            
            let item = month + "/2018"
            self.listDate.append(item)
            self.listShow.append(false)
        }
    }
    
    func initListDay() {
        for _ in 0...8 {
            self.listShowDay.append(false)
        }
    }
    
    func formatDate(date: Date) -> String? {
        let formatDate = DateFormatter()
        formatDate.dateFormat = "MM/yyyy"
        
        let dateText = formatDate.string(from: date)
        
        return dateText
    }
    
}

// MARK: collection list date name
extension ListProductViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listDate.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dateListCell", for: indexPath) as! DateListCollectionViewCell
        
        cell.lbNameDate.text = self.listDate[indexPath.row]
        
        if self.listShow[indexPath.row] {
            cell.selectedCell()
        } else {
            cell.initCell()
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let widthCell = UIScreen.main.bounds.width / 3
        let heightCell = self.listDateCollectionView.bounds.height
        
        return CGSize(width: widthCell, height: heightCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.changeDate(indexRow: indexPath.row)
    }
    
    func changeDate(indexRow: Int) {
        for i in 0..<self.listShow.count {
            self.listShow[i] = false
        }
        
        self.listShow[indexRow] = !self.listShow[indexRow]
        self.listDateCollectionView.scrollToItem(at: IndexPath(row: indexRow, section: 0), at: .centeredHorizontally, animated: true)
        self.listDateCollectionView.reloadData()
    }
    
}

extension ListProductViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.listShowDay[section] {
            return 2
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let headerView = HeaderProductView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        headerView.lbNameDate.text = "0\(section + 1)" + "/09/2018"
        headerView.index = section
        headerView.delegate = self
        view.addSubview(headerView)
        
        return view
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
}

// MARK: add delegate cell

extension ListProductViewController: showListProductDelegate {
    
    func selectShow(indexSection: Int) {
        self.listShowDay[indexSection] = !self.listShowDay[indexSection]
        self.tableView.reloadSections( IndexSet(integer: indexSection), with: .automatic)
        print(indexSection)
    }
}
